// Reverse a String
function reverseString(str) {
  var data = str.split('');
  var reversed = data.reduce(function(acc, curr) {
//     console.log(acc);
    acc.unshift(curr);
    return acc;
  }, []);
//   console.log(reversed.join(''));
  return reversed.join('');
}

reverseString("hello");

// Factorialize a Number
function factorialize(num) {
  var value = 1;
  for (var i = 1; i <= num; i++) {
    value *= i;
  }
  return value;
}

factorialize(5);

// Check for Palindromes
function palindrome(str) {
  var rx = /\W+/g;
  var sanitized = str.toLowerCase().replace(rx, '').replace(/_/g, '');
  var reversed = sanitized.split('').reverse().join('');

  console.log('l', sanitized, reversed);
  return sanitized === reversed;
}

palindrome("eye");

// Find the Longest Word in a String
function findLongestWord(str) {
  var words = str.split(' ');
  var longest = words.reduce(function(acc, curr) {
    if (curr.length > acc.length) {
      return curr;
    } else {
      return acc;
    }
  }, '');
//   console.log(str, longest);
  return longest.length;
}

findLongestWord("The quick brown fox jumped over the lazy dog");

// Title Case a Sentence
function titleCase(str) {
  var words = str.split(' ');
  var capWords = words.map(function(word) {
    var first = word[0];
    var rest = word.slice(1);
    var capWord = first.toUpperCase() + rest.toLowerCase();
    return capWord;
  });
  var result = capWords.join(' ');
  console.log(str, ' -> ', result);
  return result;
}

titleCase("I'm a little tea pot");

// Return Largest Numbers in Arrays
function largestOfFour(arr) {
  var result = arr.map(function(subArray) {
    return subArray.reduce(function(acc, curr) {
      if (curr > acc) {
        return curr;
      } else {
        return acc;
      }
    });
  });
  console.log(arr, ' -> ', result);
  return result;
}

largestOfFour([[4, 5, 1, 3], [13, 27, 18, 26], [32, 35, 37, 39], [1000, 1001, 857, 1]]);

Confirm the Ending
function confirmEnding(str, target) {

  var ending = str.substring(str.length - target.length);
  console.log([str, target, ending].join('|'));
  return ending === target;
}

confirmEnding("Bastian", "n");

// Repeat a string repeat a string
function repeatStringNumTimes(str, num) {
  var out = [];
  if (num > 0) {
    for (var i = 0; i < num; i++) {
      out.push(str);
    }
  }
  var outStr = out.join('');
  return outStr;
}

repeatStringNumTimes("abc", 3);

// Truncate a string
function truncateString(str, num) {
  // Clear out that junk in your trunk
  var outStr = str;
  var strLen = str.length;
  if (strLen > num) {
    var sliceAmount = (num > 3) ? num - 3 : num;
    outStr = outStr.slice(0, sliceAmount);
    outStr += '...';
  }
//   console.log([str, num, outStr].join('|'));
  return outStr;
}

truncateString("A-tisket a-tasket A green and yellow basket", 11);

// Chunky Monkey
function chunkArrayInGroups(arr, size) {
  var acc = {
    'out': [],
    'curr': [],
  };
  arr.reduce(function(acc, curr) {
    acc.curr.push(curr);
    if (acc.curr.length === size) {
      acc.out.push(acc.curr);
      acc.curr = [];
    }
    return acc;
  }, acc);
  if (acc.curr.length > 0) {
    acc.out.push(acc.curr);
    acc.curr = [];
  }
  console.log([arr, size, acc.out].join('|'));
  console.log(acc.out);
  return acc.out;
}

chunkArrayInGroups(["a", "b", "c", "d"], 2);

// Slasher Flick
function slasher(arr, howMany) {
  // it doesn't always pay to be first
  return arr.slice(howMany);
}

slasher([1, 2, 3], 2);

// Mutations
function mutation(arr) {
  var first = arr[0].toLowerCase();
  var second = arr[1].toLowerCase();
  var allFound = true;
  for (var i = 0; i < second.length; i++) {
    var c = second[i];
    if (first.indexOf(c) === -1) {
      allFound = false;
    }
  }

  console.log([arr, allFound].join('|'));
  return allFound;
}

mutation(["hello", "hey"]);

// Falsy Bouncer

function bouncer(arr) {
  var trueElements = arr.reduce(function(acc, curr) {
    if (curr) {
      acc.push(curr);
    }
    return acc;
  }, []);
//   console.log(arr, trueElements);
  return trueElements;
}

bouncer([7, "ate", "", false, 9]);

// Seek and Destroy
function destroyer(arr) {
  // Remove all the values
  var other = [];
  for (var i = 1; i < arguments.length; i++) {
    other.push(arguments[i]);
  }
  console.log(arr, other);
  var out = arr.filter(function(x) {
    console.log('check', x, other.indexOf(x));
    return other.indexOf(x) === -1;
  });
  console.log(out);
  return out;
}

destroyer([1, 2, 3, 1, 2, 3], 2, 3);

// Where do I belong
function getIndexToIns(arr, num) {
  // Find my place in this sorted array.
  var sorted = arr.sort(function(a, b) { return a - b; });
  var index = sorted.reduce(function(acc, curr, currIdx) {
    if (acc === null) {
      if (num <= curr) {
        acc = currIdx;
      }
    }
    return acc;
  }, null);
  if (index === null) {
    index = sorted.length;
  }
  console.log(sorted, num, index);
  return index;
}

getIndexToIns([40, 60], 50);

// Caesars Cipher
function rot13(str) { // LBH QVQ VG!
  var alphaRegex = /[A-Z]/;
  var data = str.split('');
  var out = data.map(function(x) {
    if (alphaRegex.test(x)) {
      var charCode = x.charCodeAt(0) - 65 + 13;
      charCode %= 26;
      return String.fromCharCode(charCode + 65);
    } else {
      return x;
    }
  });
  var result = out.join('');
  console.log([str, result].join(' -> '));
  return result;
}

// Change the inputs below to test
rot13("SERR PBQR PNZC");
